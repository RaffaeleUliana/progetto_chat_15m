package com.lez33.Chat.connessione;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnettoreDB {

	private Connection conn;
	private static ConnettoreDB ogg_connessione;
	
	public static ConnettoreDB getIstanza() {
		if(ogg_connessione == null) {
			ogg_connessione = new ConnettoreDB();
			return ogg_connessione;
		}
		else {
			return ogg_connessione;
		}
	}
	
	public Connection getConnessione() throws SQLException{
		if(conn == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("127.0.0.1");
			dataSource.setPortNumber(3306);
			dataSource.setUser("root");
			dataSource.setPassword("toor");
			dataSource.setUseSSL(false);
			dataSource.setDatabaseName("lez_33_chat");
			dataSource.setAllowPublicKeyRetrieval(true);
			
			conn = dataSource.getConnection();
			return conn;
		}
		else {
			return conn;
		}
	}
	
}