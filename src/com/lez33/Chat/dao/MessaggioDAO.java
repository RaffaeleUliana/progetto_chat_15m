package com.lez33.Chat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.lez33.Chat.classi.Messaggio;
import com.lez33.Chat.connessione.ConnettoreDB;



public class MessaggioDAO {
	public ArrayList<Messaggio> findAll() throws SQLException{
		ArrayList<Messaggio> elenco = new ArrayList<Messaggio>();
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT idMessaggio, testo, dataInvio, codiceUtente FROM messaggio;";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Messaggio temp = new Messaggio();
			temp.setIdMessaggio(risultato.getInt(1));
			temp.setTesto(risultato.getString(2));
			temp.setDataInvio(risultato.getTimestamp(3).toLocalDateTime());
			temp.setCodiceUtente(risultato.getString(4));
			elenco.add(temp);
		}
		
		return elenco;
	}
	
	public Messaggio getById(Integer id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT idMessaggio, testo, dataInvio, codiceUtente FROM messaggio WHERE idMessaggio = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

       	Messaggio temp = new Messaggio();
       	temp.setIdMessaggio(risultato.getInt(1));
		temp.setTesto(risultato.getString(2));
		temp.setDataInvio(risultato.getTimestamp(3).toLocalDateTime());
		temp.setCodiceUtente(risultato.getString(4));
       	
       	return temp;
	}
	
	
	
	
	
	
	public void insert(Messaggio t) throws SQLException {
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();

   		String query = "INSERT INTO messaggio (testo, dataInvio, codiceUtente) VALUES (?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getTesto());
       	ps.setTimestamp(2, Timestamp.valueOf(t.getDataInvio()));
       	ps.setString(3, t.getCodiceUtente());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setIdMessaggio(risultato.getInt(1));
		
	}

	
	
	
	public boolean delete(Messaggio t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM messaggio WHERE idMessaggio = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getIdMessaggio());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

}
