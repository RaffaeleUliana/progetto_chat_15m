package com.lez33.Chat.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lez33.Chat.classi.Utente;
import com.lez33.Chat.connessione.ConnettoreDB;


public class UtenteDAO{

	public Utente getByCodice(String varCodice) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT 	idUtente, codice, nome, cognome, ruolo, nickname, pass FROM utente WHERE codice=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, varCodice);

		ResultSet risultato = ps.executeQuery();
		risultato.next();

		Utente temp = new Utente();
		temp.setId(risultato.getInt(1));
		temp.setCodice(risultato.getString(2));
		temp.setNome(risultato.getString(3));
		temp.setCognome(risultato.getString(4));
		temp.setRuolo(risultato.getString(5));
		temp.setNickname(risultato.getString(6));
		temp.setPassword(risultato.getString(7));

		return temp;
	}

	public ArrayList<Utente> getAll() throws SQLException {
		ArrayList<Utente> elenco = new ArrayList<Utente>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT 	idUtente, codice, nome, cognome, ruolo, FROM utente WHERE 1=1";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {
			Utente temp = new Utente();
			temp.setId(risultato.getInt(1));
			temp.setCodice(risultato.getString(2));
			temp.setNome(risultato.getString(3));
			temp.setCognome(risultato.getString(4));
			temp.setRuolo(risultato.getString(5));
			temp.setNickname(risultato.getString(6));
			temp.setPassword(risultato.getString(7));
			elenco.add(temp);
		}

		return elenco;
	}

	public void insert(Utente t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO utente (codice, nome, cognome, ruolo, nickname, pass) VALUES (?,?,?,?,?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getCodice());
		ps.setString(2, t.getNome());
		ps.setString(3, t.getCognome());
		ps.setString(4, t.getRuolo());
		ps.setString(5, t.getNickname());
		ps.setString(6, t.getPassword());
		

		ps.executeUpdate();
//		ResultSet risultato = ps.getGeneratedKeys();
//		risultato.next();

	}

	public boolean delete(Utente t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM utente WHERE utenteId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}

	public Utente update(Utente t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE utente SET codice = ?, nome = ?, cognome = ?, ruolo = ?, nickname=?, pass=? WHERE idUtente = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, t.getCodice());
		ps.setString(2, t.getNome());
		ps.setString(3, t.getCognome());
		ps.setString(4, t.getRuolo());
		ps.setString(5, t.getNickname());
		ps.setString(6, t.getPassword());
		ps.setInt(7, t.getId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return getByCodice(t.getCodice());

		return null;
	}
	
	public String checkRuolo(Utente objUtente) throws SQLException {
		String ruolo = null;
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT ruolo FROM utente WHERE nickname = ? AND pass = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, objUtente.getNickname());
       	ps.setString(2, objUtente.getPassword());
       	
       	ResultSet risultato = ps.executeQuery();
       	if(risultato.next()) {
       		ruolo = risultato.getString(1);
       		return ruolo;
       	}else {
       		return ruolo;
       	}
	}
	
	public String getCodiceByNickname(String nickname) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT codice FROM utente WHERE nickname=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, nickname);

		ResultSet risultato = ps.executeQuery();
		risultato.next();
		String codiceUtente = risultato.getString(1);

		return codiceUtente;
	}

}
