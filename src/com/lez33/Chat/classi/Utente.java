package com.lez33.Chat.classi;

import java.util.ArrayList;

public class Utente {
	private Integer id;
	private String codice;
	private String nome;
	private String cognome;
	private String ruolo;
	private String nickname;
	private String password;
	private ArrayList<Messaggio> elencoMessaggi;

	public Utente() {	
	}

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	

	public ArrayList<Messaggio> getElencoMessaggi() {
		return elencoMessaggi;
	}

	public void setElencoMessaggi(ArrayList<Messaggio> elencoMessaggi) {
		this.elencoMessaggi = elencoMessaggi;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}