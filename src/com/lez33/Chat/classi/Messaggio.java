package com.lez33.Chat.classi;

import java.time.LocalDateTime;

public class Messaggio {
	private Integer idMessaggio;
	private String testo;
	private LocalDateTime dataInvio;
	private String codiceUtente;
	
	public Messaggio() {
		
	}

	public Integer getIdMessaggio() {
		return idMessaggio;
	}

	public void setIdMessaggio(Integer idMessaggio) {
		this.idMessaggio = idMessaggio;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

	public LocalDateTime getDataInvio() {
		return dataInvio;
	}

	public void setDataInvio() {
		this.dataInvio = LocalDateTime.now();
	}
	
	public void setDataInvio(LocalDateTime time) {
		this.dataInvio = time;
	}

	public String getCodiceUtente() {
		return codiceUtente;
	}

	public void setCodiceUtente(String codiceUtente) {
		this.codiceUtente = codiceUtente;
	}

	@Override
	public String toString() {
		return "Messaggio [codiceUtente=" + codiceUtente + ", testo=" + testo + ", dataInvio=" + dataInvio + "]";
	}
	
}
