package com.lez33.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lez33.Chat.classi.Messaggio;
import com.lez33.Chat.dao.MessaggioDAO;

/**
 * Servlet implementation class ScriviMessaggio
 */
@WebServlet("/scrivimessaggio")
public class ScriviMessaggio extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String testoMessaggio = request.getParameter("input_messaggio") != null ? request.getParameter("input_messaggio") : "";
		HttpSession sessione = request.getSession();
	    String codiceUtente = (String)sessione.getAttribute("codice") != null ? (String)sessione.getAttribute("codice") : "";
		
		Messaggio messTemp = new Messaggio();
		messTemp.setDataInvio();
		messTemp.setCodiceUtente(codiceUtente);
		messTemp.setTesto(testoMessaggio);
		MessaggioDAO messDAO = new MessaggioDAO();
		try {
			messDAO.insert(messTemp);
		} catch (SQLException e) {
			e.printStackTrace();
			response.sendRedirect("errore.jsp?tipo_errore=SQL_ERROR");
		}
		response.sendRedirect("chat.jsp");
		
	}

}
