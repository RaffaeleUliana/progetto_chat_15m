package com.lez33.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lez33.Chat.classi.Utente;
import com.lez33.Chat.dao.UtenteDAO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/verificalogin")
public class VerificaLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String user = request.getParameter("input_username") != null ? request.getParameter("input_username") : "";
		String pass = request.getParameter("input_password") != null ? request.getParameter("input_password") : "";
		
		Utente utente = new Utente();
		utente.setNickname(user);
		utente.setPassword(pass);
		
		UtenteDAO utente_dao = new UtenteDAO();
		String ruolo = null;
		try {
			ruolo = utente_dao.checkRuolo(utente);
			if(ruolo != null) {
				String codiceUtente = utente_dao.getCodiceByNickname(user);
				if(ruolo.equalsIgnoreCase("admin")) {
					HttpSession sessione = request.getSession();
					sessione.setAttribute("ruolo", ruolo);
					sessione.setAttribute("codice", codiceUtente);
					response.sendRedirect("admin.jsp");
				}else {
					HttpSession sessione = request.getSession();
					sessione.setAttribute("ruolo", ruolo);
					sessione.setAttribute("codice", codiceUtente);
					response.sendRedirect("chat.jsp");
				}
			}else {
				response.sendRedirect("errore.jsp?tipo_errore=NO_USER");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			response.sendRedirect("errore.jsp?tipo_errore=SQL_ERROR");
		}
		
		
	}

}
