package com.lez33.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lez33.Chat.classi.Utente;
import com.lez33.Chat.dao.UtenteDAO;

/**
 * Servlet implementation class Registrazione
 */
@WebServlet("/registrazione")
public class Registrazione extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nome = request.getParameter("input_nome") != null ? request.getParameter("input_nome") : "";
		String cognome = request.getParameter("input_cognome") != null ? request.getParameter("input_cognome") : "";
		String user = request.getParameter("input_username_new") != null ? request.getParameter("input_username_new") : "";
		String pass = request.getParameter("input_password_new") != null ? request.getParameter("input_password_new") : "";
		
		Utente utente = new Utente();
		String codiceUnivoco = UUID.randomUUID().toString().replace("-", "").substring(0, 12).toUpperCase();
		utente.setCodice(codiceUnivoco);
		utente.setNome(nome);
		utente.setCognome(cognome);
		utente.setNickname(user);
		utente.setPassword(pass);
		utente.setRuolo("simple");
		
		UtenteDAO utDAO = new UtenteDAO();
		try {
			utDAO.insert(utente);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.sendRedirect("errore.jsp?tipo_errore=SQL_ERROR");
		}
		response.sendRedirect("Login.jsp");
	}

}
