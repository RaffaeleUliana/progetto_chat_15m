<%@ page import="com.lez33.*"%>
<%@ page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	// abbiamo tolto momentaneamente i controlli
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="shortcut icon" href="#">
<title>Login</title>
</head>
<body>
	<div class="container">
		<div class="row mt-5">
			<div class="col-md-3"></div>
			<div class="col-md-6 text-center">
				<h1>CHAT</h1>
				<p>Effettua l'accesso alla chat inserendo nome utente e password</p>
			</div>
			<div class="col-md-3"></div>
		</div>
		<div class="row mt-5">
			<div class="col-3"></div>
			<div class="col-6">
				<form name="form_login" action="verificalogin" method="POST">
					<div class="form-group">
						<label for="input_username">Username</label> <input type="text"
							class="form-control" name="input_username" />
					</div>
					<div class="form-group">
						<label for="input_password">Password</label> <input
							type="password" class="form-control" name="input_password" />
					</div>
					<button type="button" class="btn btn-success" name="btn_login">LogIn</button>
				</form>


				<form name="form_registrazione" action="registrazione" method="POST">
					<div class="form-group">
						<label for="input_nome">Nome</label> <input type="text"
							class="form-control" name="input_nome" />
					</div>
					<div class="form-group">
						<label for="input_cognome">Cognome</label> <input type="text"
							class="form-control" name="input_cognome" />
					</div>
					<div class="form-group">
						<label for="input_username_new">Username</label> <input
							type="text" class="form-control" name="input_username_new" />
					</div>
					<div class="form-group">
						<label for="input_password_new">Password</label> <input
							type="password" class="form-control" name="input_password_new" />
					</div>
					<button type="button" class="btn btn-success"
						name="btn_registrazione">Registrati</button>
				</form>
			</div>
			<div class="col-3"></div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script src="js/script.js"></script>
</body>
</html>