

$(document).ready(
	function() {

		$("button[name=btn_registrazione]").click(
			function() {
				let varNome = $("input[name=input_nome]").val();
				let varCognome = $("input[name=input_cognome]").val();
				let varUsername = $("input[name=input_username_new]").val();
				let varPassword = $("input[name=input_password_new]").val();

				if (varNome == "" || varCognome == "" || varUsername == "" || varPassword == "") {
					alert("Errore, nessuno dei campi atti alla registrazione può rimanere vuoto!");
					return;
				}

				$("form[name=form_registrazione]").submit();
			}
		);

		$("button[name=btn_login]").click(
			function() {

				let usr = $("input[name=input_username]").val();
				let pwd = $("input[name=input_password]").val();

				if (usr == "") {
					alert("Errore, lo Username non può essere vuoto!");
					return;
				}

				if (pwd == "") {
					alert("Errore, la Password non può essere vuoto!");
					return;
				}

				$("form[name=form_login]").submit();
			}
		);

		$("#invia-messaggio").click(
			function() {
				let testoMessaggio = $("#input_messaggio").val();
				$.ajax(
					{
						url: "http://localhost:8080/Chat2/scrivimessaggio",
						method: "POST",
						data: {
							input_messaggio: testoMessaggio
						},
						success: function(ris) {
							console.log("Ho inserito il messaggio")
						},
						error: function(errore) {
							console.log(errore);
						}
					}
				);
			}
		);
	}
);