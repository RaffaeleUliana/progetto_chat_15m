<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="java.lang.reflect.Type"%>
<%@page import="com.lez33.Chat.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.google.gson.Gson"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
    	HttpSession sessione = request.getSession();
    	String ruolo_utente = "";
    	if((String)sessione.getAttribute("ruolo") != null){
    		ruolo_utente = (String)sessione.getAttribute("ruolo");
    	}
    	
    	// if(!ruolo_utente.equals("SIMPLE"))
    	//	response.sendRedirect("errore.jsp?tipo_errore=NOT_ALLOWED");
    %>


<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="file.css" >
        <title>Chat</title>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-2">

                    <div class="row mt-2">
                        <a class="navbar-brand">GREENchat</a>
                    </div>

                    <div class="row mt-2">
                        <div class="col">
                            <textarea  id="area-messaggi" ></textarea>
                        </div>
                        
                    </div>
                    <div class="row mt-2">
                        
                            <input type="text" class="form-control" id="input_messaggio" placeholder="scrivi il messaggio">
  
                        
                        
                    </div>
                    
                    
                    <div class="row mt-2">
                        <div class="col">
                            <button type="button" class="btn btn-success" id="invia-messaggio">Invia</button>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="col-md-10">
                    
                
            </div>
            
        </div>
	
    </body>
</html>


<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="js/script.js"></script>
</body>
</html>	
	