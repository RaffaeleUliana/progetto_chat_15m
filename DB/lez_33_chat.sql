DROP DATABASE IF EXISTS lez_33_chat;
CREATE DATABASE lez_33_chat;
USE lez_33_chat;

CREATE TABLE utente(
	idUtente INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    codice VARCHAR(20) NOT NULL UNIQUE,
    nome VARCHAR(30) NOT NULL,
    cognome VARCHAR(30) NOT NULL,
    nickname VARCHAR(30) NOT NULL UNIQUE,
    pass VARCHAR(20) NOT NULL,
    ruolo VARCHAR(30) NOT NULL CONSTRAINT CHECK (ruolo IN("admin", "simple"))
    
);

CREATE TABLE messaggio(
	idMessaggio INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    testo TEXT NOT NULL,
    dataInvio TIMESTAMP NOT NULL,
    codiceUtente VARCHAR(20) NOT NULL,
    
    FOREIGN KEY (codiceUtente) REFERENCES utente(codice) ON DELETE CASCADE
);

INSERT INTO utente (codice, nome, cognome, ruolo, nickname, pass) VALUES
("AA123456", "Bubbolo", "Cazza", "admin", "Bubba", "123456"),
("AB123456", "Francesco", "Verdi", "simple", "Fro", "123456" ),
("AC123456", "Orsola", "Rossi", "simple", "Orw", "123456" ),
("AD123456", "Nicole", "Bianchi", "simple", "NicNic", "123456");

INSERT INTO messaggio (testo, dataInvio, codiceUtente) VALUES
("Molto piacere di conoscervi", "2021-03-01 08:00:00", "AA123456"),
("Evviva la mortadella", "2021-03-01 08:01:00", "AB123456"),
("Il Papa ha twittato", "2021-03-01 08:03:00", "AC123456"),
("Torna a casa Lessie", "2021-03-01 08:04:00", "AD123456");

SELECT m.testo, m.dataInvio, u.nome, u.cognome
	FROM messaggio m
    JOIN utente u ON m.codiceUtente = u.codice
    WHERE 1 = 1;
SELECT * FROM messaggio;

SELECT * FROM utente WHERE 1=1;
